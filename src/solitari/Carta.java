package solitari;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Carta {
	
	public static ArrayList<String> ayu(String etiqueta) {
		
		ArrayList<String> Cartes = new ArrayList<String>();
	
		Cartes.add("1Treboles");
		Cartes.add("2Treboles");
		Cartes.add("3Treboles");
		Cartes.add("4Treboles");
		Cartes.add("5Treboles");
		Cartes.add("6Treboles");
		Cartes.add("7Treboles");
		Cartes.add("8Treboles");
		Cartes.add("9Treboles");
		Cartes.add("10Treboles");
		Cartes.add("JotaDeTreboles");
		Cartes.add("DamaDeTreboles");
		Cartes.add("ReyDeTreboles");
		
		Cartes.add("1Diamantes");
		Cartes.add("2Diamantes");
		Cartes.add("3Diamantes");
		Cartes.add("4Diamantes");
		Cartes.add("5Diamantes");
		Cartes.add("6Diamantes");
		Cartes.add("7Diamantes");
		Cartes.add("8Diamantes");
		Cartes.add("9Diamantes");
		Cartes.add("10Diamantes");
		Cartes.add("JotaDeDiamante");
		Cartes.add("DamaDeDiamantes");
		Cartes.add("ReyDeDiamantes");
		
		Cartes.add("1Corazones");
		Cartes.add("2Corazones");
		Cartes.add("3Corazones");
		Cartes.add("4Corazones");
		Cartes.add("5Corazones");
		Cartes.add("6Corazones");
		Cartes.add("7Corazones");
		Cartes.add("8Corazones");
		Cartes.add("9Corazones");
		Cartes.add("10Corazones");
		Cartes.add("JotaDeCorazones");
		Cartes.add("DamaDeCorazones");
		Cartes.add("ReyDeTCorazones");
		
		Cartes.add("1Picas");
		Cartes.add("2Picas");
		Cartes.add("3Picas");
		Cartes.add("4Picas");
		Cartes.add("5Picas");
		Cartes.add("6Picas");
		Cartes.add("7Picas");
		Cartes.add("8Picas");
		Cartes.add("9Picas");
		Cartes.add("10Picas");
		Cartes.add("JotaDePicas");
		Cartes.add("DamaDePicas");
		Cartes.add("ReyDePicas");
		
		Collections.shuffle(Cartes);
		
		
		
		return Cartes;
		
	}

}
